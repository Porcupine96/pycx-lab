# import importlib
# exp = importlib.import_module('net-voter')

import net_voter
from net_voter import init, step
from collections import Counter

from time import sleep

init()


def nodes_per_state(g):
    states = [g.node[nd]['state'] for nd in g.nodes_iter()]
    counter = Counter(states)
    return counter


def go():
    steps = 0
    converged = False
    while not converged:
        if len(nodes_per_state(net_voter.g)) == 1:
            converged = True
        step()
        steps += 1

    print 'Converged after: ' + str(steps)

go()

