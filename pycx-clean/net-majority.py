# Simple Network Dynamics simulator in Python
#
# *** Majority Rule on a Social Network ***
#
# Copyright 2011-2012 Hiroki Sayama
# sayama@binghamton.edu

import matplotlib
matplotlib.use('TkAgg')

import pylab as PL
import networkx as NX
import random as RD
import numpy as np

RD.seed()

col = {0:'w', 1:'k'}
one_prob = 0.5

def init():
    global one_prob, time, network, nextNetwork, positions

    time = 0

    network = NX.random_regular_graph(4, 100)
    for n in network.nodes_iter():
        #network.node[n]['state'] = RD.choice([0, 1])
        network.node[n]['state'] = np.random.choice([0, 1], p=[1. - one_prob, one_prob])

    nextNetwork = network.copy()

    positions = NX.spring_layout(network)

def draw():
    PL.cla()
    NX.draw(network, pos = positions, node_color = [col[network.node[n]['state']] for n in network.nodes_iter()])
    PL.axis('image')
    PL.title('t = ' + str(time))

def step():
    global time, network, nextNetwork

    time += 1

    for n in network.nodes_iter():
        total = network.node[n]['state']
        nbs = network.neighbors(n)
        for nb in nbs:
            total += network.node[nb]['state']
        if total * 2 > len(nbs) + 1:
            nextNetwork.node[n]['state'] = 1
        elif total * 2 < len(nbs) + 1:
            nextNetwork.node[n]['state'] = 0
        else:
            nextNetwork.node[n]['state'] = network.node[n]['state']

    network, nextNetwork = nextNetwork, network


def test_setter(val = one_prob):
    global one_prob
    one_prob = val
    return val


import pycxsimulator
pycxsimulator.GUI(parameterSetters=[test_setter]).start(func=[init,draw,step])
