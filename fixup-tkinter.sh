#!/bin/bash

TK_LIBRARY=/usr/lib/python2.7/lib-tk:/usr/lib/python2.7/site-packages/PIL:/usr/lib
TKPATH=/usr/lib/python2.7/lib-tk:/usr/lib/python2.7/site-packages/PIL:/usr/lib
TCL_LIBRARY=/usr/lib
export TCL_LIBRARY TK_LIBRARY TKKPATH

export PYTHONPATH=/usr/lib/python2.7/lib-tk:/usr/lib/python2.7/lib-stdwin:/usr/lib/python2.7/lib-dynload:/usr/lib:.
