import matplotlib
matplotlib.use('TkAgg')

import matplotlib.pyplot as plt

import pylab
import random
import numpy as np
import networkx as nx

IN = 0
OUT = 1

n = 10
m = 1000
p_min = 0.0
p_max = 0.1
f = 1.01

def contribution(money):
    a = 1000.
    r = random.randint(int(p_min * a), int(p_max * a))
    return (r / a) * money


def init():
    global G, wallet, step_type

    step_type = IN
    G = nx.star_graph(n)
    G.pos = nx.spring_layout(G)
    wallet = np.ones(n + 1) * m


def draw():
    pylab.cla()

    # draw graph
    nx.draw(G, node_color=wallet, cmap=plt.cm.Blues,
            pos=G.pos, vmin=0, vmax=m,
            node_size=1500, with_labels=False)

    #draw labels (money)
    for i in range(n+1):
        x, y = G.pos[i]
        label = '%.2f' % (wallet[i],)
        plt.text(x, y+0.05, s=label, fontsize=20,
                bbox=dict(facecolor='none', edgecolor='black',boxstyle='round,pad=1'),
                horizontalalignment='center')


def step():
    global wallet, step_type, pot

    if step_type == IN:
        contributions = np.array([contribution(m) for m in wallet[1:]])
        pot = np.sum(contributions)
        wallet = np.concatenate([[pot], wallet[1:] - contributions])
        step_type = OUT
    else:
        income = (f * pot) / n
        wallet[0] = pot = 0
        wallet[1:] = income + wallet[1:]
        step_type = IN



import pycxsimulator
pycxsimulator.GUI().start(func=[init,draw,step])

